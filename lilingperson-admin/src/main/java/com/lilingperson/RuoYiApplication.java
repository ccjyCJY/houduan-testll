package com.lilingperson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class RuoYiApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  醴陵人才网站启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  _       _   _   _                   ____                                      \n" +
                " | |     (_) | | (_)  _ __     __ _  |  _ \\    ___   _ __   ___    ___    _ __  \n" +
                " | |     | | | | | | | '_ \\   / _` | | |_) |  / _ \\ | '__| / __|  / _ \\  | '_ \\ \n" +
                " | |___  | | | | | | | | | | | (_| | |  __/  |  __/ | |    \\__ \\ | (_) | | | | |\n" +
                " |_____| |_| |_| |_| |_| |_|  \\__, | |_|      \\___| |_|    |___/  \\___/  |_| |_|\n" +
                "                              |___/                                             ");
    }
}
