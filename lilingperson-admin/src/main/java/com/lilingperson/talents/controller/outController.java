package com.lilingperson.talents.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lilingperson.common.annotation.Log;
import com.lilingperson.common.core.controller.BaseController;
import com.lilingperson.common.core.domain.AjaxResult;
import com.lilingperson.common.enums.BusinessType;
import com.lilingperson.talents.domain.out;
import com.lilingperson.talents.service.IoutService;
import com.lilingperson.common.utils.poi.ExcelUtil;
import com.lilingperson.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 管理Controller
 *
 * @author cjy
 * @date 2024-10-26
 */
@Api(tags = {"管理"})
@RestController
@RequestMapping("/talents/out")
public class outController extends BaseController
{
    @Autowired
    private IoutService outService;

    /**
     * 查询管理列表
     */
    @ApiOperation("查询管理列表")
    @PreAuthorize("@ss.hasPermi('talents:out:list')")
    @GetMapping("/list")
    public TableDataInfo list(out out)
    {
        startPage();
        List<out> list = outService.selectoutList(out);
        return getDataTable(list);
    }

    /**
     * 导出管理列表
     */
    @ApiOperation("导出管理列表")
    @PreAuthorize("@ss.hasPermi('talents:out:export')")
    @Log(title = "管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, out out)
    {
        List<out> list = outService.selectoutList(out);
        ExcelUtil<out> util = new ExcelUtil<out>(out.class);
        util.exportExcel(response, list, "管理数据");
    }

    /**
     * 获取管理详细信息
     */
    @ApiOperation("获取管理详细信息")
    @PreAuthorize("@ss.hasPermi('talents:out:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(outService.selectoutById(id));
    }

    /**
     * 新增管理
     */
    @ApiOperation("新增管理")
    @PreAuthorize("@ss.hasPermi('talents:out:add')")
    @Log(title = "管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody out out)
    {
        return toAjax(outService.insertout(out));
    }

    /**
     * 修改管理
     */
    @ApiOperation("修改管理")
    @PreAuthorize("@ss.hasPermi('talents:out:edit')")
    @Log(title = "管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody out out)
    {
        return toAjax(outService.updateout(out));
    }

    /**
     * 删除管理
     */
    @ApiOperation("删除管理")
    @PreAuthorize("@ss.hasPermi('talents:out:remove')")
    @Log(title = "管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(outService.deleteoutByIds(ids));
    }

    /**
     * 文件导入后端代码
     */

    @PreAuthorize("@ss.hasPermi('talents:out:add')")
    @Log(title = "管理", businessType = BusinessType.IMPORT)
    @PostMapping("/import")
    public AjaxResult excelImport(MultipartFile file)throws Exception{
        ExcelUtil<out> util =new ExcelUtil<out>(out.class);
        List<out> outList = util.importExcel(file.getInputStream());

        return toAjax(outService.batchInsert(outList));
    }



}
