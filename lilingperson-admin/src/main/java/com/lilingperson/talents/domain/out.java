package com.lilingperson.talents.domain;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.lilingperson.common.annotation.Excel;
import com.lilingperson.common.core.domain.BaseEntity;

/**
 * 管理对象 lilingexpatriatetalent
 * 
 * @author cjy
 * @date 2024-10-26
 */
@ExcelIgnoreUnannotated//表示在导出excel时，忽略没有任何注解的字段
@ColumnWidth(16)//列的宽度
@HeadRowHeight(14)//注解用于设置表头行的高度
@HeadFontStyle(fontHeightInPoints = 11)//注解用于设置表头的字体样式
public class out extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 在外人才Id */
    @Excel(name = "在外人才Id")
    @ExcelProperty("在外人才Id")
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    @ExcelProperty("姓名")
    private String name;

    /** 手机号 */
    @Excel(name = "手机号")
    @ExcelProperty("手机号")
    private String number;

    /** 身份证号 */
    @Excel(name = "身份证号")
    @ExcelProperty("身份证号")
    private String idnmuber;

    /** 学历 */
    @Excel(name = "学历")
    @ExcelProperty("学历")
    private String xueli;

    /** 工作单位 */
    @Excel(name = "工作单位")
    @ExcelProperty("工作单位")
    private String workutil;

    /** 人才类型 */
    @Excel(name = "人才类型")
    @ExcelProperty("人才类型")
    private String type;

    /** 城市 */
    @Excel(name = "城市")
    @ExcelProperty("城市")
    private String city;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setIdnmuber(String idnmuber) 
    {
        this.idnmuber = idnmuber;
    }

    public String getIdnmuber() 
    {
        return idnmuber;
    }
    public void setXueli(String xueli) 
    {
        this.xueli = xueli;
    }

    public String getXueli() 
    {
        return xueli;
    }
    public void setWorkutil(String workutil) 
    {
        this.workutil = workutil;
    }

    public String getWorkutil() 
    {
        return workutil;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("number", getNumber())
            .append("idnmuber", getIdnmuber())
            .append("xueli", getXueli())
            .append("workutil", getWorkutil())
            .append("type", getType())
            .append("city", getCity())
            .append("remark", getRemark())
            .toString();
    }
}
