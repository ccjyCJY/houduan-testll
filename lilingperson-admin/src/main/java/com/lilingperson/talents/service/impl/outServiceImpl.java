package com.lilingperson.talents.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.lilingperson.talents.mapper.outMapper;
import com.lilingperson.talents.domain.out;
import com.lilingperson.talents.service.IoutService;

/**
 * 管理Service业务层处理
 * 
 * @author cjy
 * @date 2024-10-26
 */
@Service
public class outServiceImpl implements IoutService 
{
    @Autowired
    private outMapper outMapper;

    /**
     * 查询管理
     * 
     * @param id 管理主键
     * @return 管理
     */
    @Override
    public out selectoutById(Long id)
    {
        return outMapper.selectoutById(id);
    }

    /**
     * 查询管理列表
     * 
     * @param out 管理
     * @return 管理
     */
    @Override
    public List<out> selectoutList(out out)
    {
        return outMapper.selectoutList(out);
    }

    /**
     * 新增管理
     * 
     * @param out 管理
     * @return 结果
     */
    @Override
    public int insertout(out out)
    {
        return outMapper.insertout(out);
    }

    /**
     * 修改管理
     * 
     * @param out 管理
     * @return 结果
     */
    @Override
    public int updateout(out out)
    {
        return outMapper.updateout(out);
    }

    /**
     * 批量删除管理
     * 
     * @param ids 需要删除的管理主键
     * @return 结果
     */
    @Override
    public int deleteoutByIds(Long[] ids)
    {
        return outMapper.deleteoutByIds(ids);
    }

    /**
     * 删除管理信息
     * 
     * @param id 管理主键
     * @return 结果
     */
    @Override
    public int deleteoutById(Long id)
    {
        return outMapper.deleteoutById(id);
    }

    /**
     * 批量新增醴籍在外人才
     *
     * @param list
     * @return 结果
     */
    @Override
    public int batchInsert(List<out> list) {
        return outMapper.batchInsert(list);
    }
}
