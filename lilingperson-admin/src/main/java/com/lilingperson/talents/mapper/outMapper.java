package com.lilingperson.talents.mapper;

import java.util.List;
import com.lilingperson.talents.domain.out;

/**
 * 管理Mapper接口
 * 
 * @author cjy
 * @date 2024-10-26
 */
public interface outMapper 
{
    /**
     * 查询管理
     * 
     * @param id 管理主键
     * @return 管理
     */
    public out selectoutById(Long id);

    /**
     * 查询管理列表
     * 
     * @param out 管理
     * @return 管理集合
     */
    public List<out> selectoutList(out out);

    /**
     * 新增管理
     * 
     * @param out 管理
     * @return 结果
     */
    public int insertout(out out);

    /**
     * 修改管理
     * 
     * @param out 管理
     * @return 结果
     */
    public int updateout(out out);

    /**
     * 删除管理
     * 
     * @param id 管理主键
     * @return 结果
     */
    public int deleteoutById(Long id);

    /**
     * 批量删除管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteoutByIds(Long[] ids);


    /**
     * 批量新增醴籍在外人才
     *
     * @param list
     * @return 结果
     */
    public   int batchInsert(List<out> list);
}
